package com.example.myapplication;

import android.app.ListActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends ListActivity  {

    final String[] colorsArray = new String[]{"Белый", "Черный", "Желтый", "Красный", "Фиолетовый",
            "Синий", "Зелёный", "Бурый", "Алый", "Туманный", "Коралловый", "Карий", "Светлый", "Синеватый", "Сиреневый"};

    private ArrayAdapter<String> mAdapter;

    private ArrayList<String> colorList = new ArrayList<>(Arrays.asList(colorsArray));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, colorList);
        setListAdapter(mAdapter);//запускаем адатер

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Toast.makeText(getApplicationContext(),
                "Вы выбрали цвет: " + l.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
    }

}